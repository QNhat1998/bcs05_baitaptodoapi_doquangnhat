const inToDo = (todos) => {
    let strHTML = '';
    todos.map(todo => {
        let{id,name,desc,isComplete} = todo
        strHTML += `<tr>
            <td>${id}</td>
            <td>${name}</td>
            <td>${desc}</td>
            <td align='center'>
                <input type='checkbox' onclick='isComplete("${id}")' ${isComplete ? 'checked' : ''}>
            </td>
            <td>
                <button onclick='deleteToDo(${id})' class='btn btn-danger'>Xóa</button>
                <button onclick='editToDo(${id})' class='btn btn-secondary'>Sửa</button>
            </td>
        </tr>`
    })
    document.getElementById('todo-list').innerHTML = strHTML
}

const layThongTinTuForm = () => {
    let name = document.getElementById('name').value,
        desc = document.getElementById('desc').value
    return{
        name,desc
    }
}

const kiemTraRong = (value,tb) => {
    console.log(tb,value)
    if(value.trim() === ''){
        document.getElementById(tb).innerHTML = 'Nội dung trống'
        return false
    }else{
        document.getElementById(tb).innerHTML = ''
        return true
    }
}

const nhapNoiDung = (name = '', desc = '') => {
    document.getElementById('name').value = name
    document.getElementById('desc').value = desc
}


