const URL = 'https://635d3a2afc2595be2656024b.mockapi.io';
let idToDo = null

let todoList = []

const fetchDataToDo = () => {
    onLoading()
    axios({
        url: `${URL}/apis/todos`,
        method: "GET"
    }).then(res => {
        offLoading();
        todoList = res.data
        inToDo(res.data)
    }).catch(err => {
        offLoading();
        console.log('err:', err)
    })
}

fetchDataToDo()

const addToDo = () => {
    let { name, desc } = layThongTinTuForm(),
        valid = true

    valid &= kiemTraRong(name, 'tbTask') & kiemTraRong(desc, 'tbDesc')

    if (valid) {
        let newToDo = { name, desc, isComplete: true }
        axios({
            url: `${URL}/apis/todos`,
            method: "POST",
            data: newToDo
        }).then(res => {
            nhapNoiDung()
            fetchDataToDo()
        }).catch(err => {
            console.log('err:', err)
        })
    }
}

const editToDo = (idEdit) => {
    let newToDo = { ...layThongTinTuForm(), isComplete: true }
    axios({
        url: `${URL}/apis/todos/${idEdit}`,
        method: "GET",
    }).then(res => {
        let { name, desc, id } = res.data
        idToDo = id
        statusBtn(true)
        nhapNoiDung(name, desc)
    }).catch(err => {
        console.log('err:', err)
    })
}

const deleteToDo = (idDel) => {
    axios({
        url: `${URL}/apis/todos/${idDel}`,
        method: "DELETE",
    }).then(res => {
        fetchDataToDo()
    }).catch(err => {
        console.log('err:', err)
    })
}

const updateToDo = () => {
    let newToDo = { ...layThongTinTuForm(), isComplete: true }
    axios({
        url: `${URL}/apis/todos/${idToDo}`,
        method: "PUT",
        data: newToDo
    }).then(res => {
        nhapNoiDung()
        statusBtn(false)
        fetchDataToDo()
    }).catch(err => {
        console.log('err:', err)
    })
}

const isComplete = (id) => {
    let index = todoList.findIndex(todo => todo.id === id),
        newToDo
    console.log(todoList[index])
    if (todoList[index].isComplete) {
        newToDo = { ...todoList[index], isComplete: false }
    } else {
        newToDo = { ...todoList[index], isComplete: true }
    }
    axios({
        url: `${URL}/apis/todos/${id}`,
        method: "PUT",
        data: newToDo
    }).then(res => {
        nhapNoiDung()
        fetchDataToDo()
    }).catch(err => {
        console.log('err:', err)
    })
}

function onLoading() {
    document.getElementById(`loading`).style.display = `flex`;
}

function offLoading() {
    document.getElementById(`loading`).style.display = `none`;
}

function statusBtn(isDisable){
    document.getElementById('update').disabled = !isDisable
    document.getElementById('add').disabled = isDisable
}

document.getElementById('update').disabled = true